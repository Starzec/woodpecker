const staticData = [
    {
        'name': 'Kamil Nowak',
        'experience': 'Expert',
        'country': 'Poland',
        'place': 'Świętokrzyski National Park',
        'title': 'Looking around',
        'photo': 'woodpecker1.jpg',
        'avatar': 'avatar1.png'
    },
    {
        'name': 'Michał Malinowski',
        'experience': 'Newbie',
        'country': 'Poland',
        'place': 'Białowieski Park Narodowy',
        'title': 'Front photo',
        'photo': 'woodpecker2.jpg',
        'avatar': 'avatar2.png'
    },
    {
        'name': 'Kasia Murarska',
        'experience': 'Intermediate',
        'country': 'Poland',
        'place': 'Biebrzański Park Narodowy',
        'title': 'Woodpecker in action',
        'photo': 'woodpecker3.jpg',
        'avatar': 'avatar3.png'
    },
    {
        'name': 'Asia Stonoga',
        'experience': 'Intermediate',
        'country': 'Poland',
        'place': 'Gorczański Park Narodowy',
        'title': 'Looking for fun',
        'photo': 'woodpecker4.jpg',
        'avatar': 'avatar4.png'
    },
    {
        'name': 'Konstancja Kowalska',
        'experience': 'Expert',
        'country': 'Poland',
        'place': 'Kampinoski Park Narodowy',
        'title': 'Woodpecker in winter',
        'photo': 'woodpecker5.jpeg',
        'avatar': 'avatar5.png'
    },
    {
        'name': 'Aleksandra Komarska',
        'experience': 'Newbie',
        'country': 'Poland',
        'place': 'Karkonoski Park Narodowy',
        'title': 'Really small hair',
        'photo': 'woodpecker6.jpg',
        'avatar': 'avatar6.png'
    }
];

export default { staticData };
