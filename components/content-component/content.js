import data from './data.js';

fetch("../components/content-component/content.html")
    .then(stream => stream.text())
    .then(text => create_content(text));

let createObservation = function (obs) {
    return `<single-observation name="${obs['name']}" experience="${obs['experience']}" 
    country="${obs['country']}" place="${obs['place']}" title="${obs['title']}" 
    photo="${obs['photo']}" avatar="${obs['avatar']}"></single-observation>`;
}

function create_content(html) {
    class Content extends HTMLElement {
        constructor() {
            super();
            let shadow = this.attachShadow({mode: 'open'});
            shadow.innerHTML = html;
        }

        connectedCallback() {
            let leftColumn = this.shadowRoot.getElementById('left-column');
            let rightColumn = this.shadowRoot.getElementById('right-column');
            data.staticData.forEach(function (obs, i) {
                if (i % 2 === 0){
                    leftColumn.insertAdjacentHTML('beforeend', createObservation(obs));
                } else {
                    rightColumn.insertAdjacentHTML('beforeend', createObservation(obs));
                }
            });
        }
    }

    window.customElements.define('content-container', Content);
}
