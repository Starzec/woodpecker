fetch("../components/content-component/single-observation/single-observation.html")
    .then(stream => stream.text())
    .then(text => create_observation(text));

const pathToPhotos = '../assets/woodpeckers/';
const pathToAvatars = '../assets/avatars/';

function create_observation(html) {
    class SingleObservation extends HTMLElement {
        constructor() {
            super();
            let shadow = this.attachShadow({mode: 'open'});
            shadow.innerHTML = html;
            this.name = this.getAttribute('name');
            this.experience = this.getAttribute('experience');
            this.country = this.getAttribute('country');
            this.place = this.getAttribute('place');
            this.title = this.getAttribute('title');
            this.photo = this.getAttribute('photo');
            this.avatar = this.getAttribute('avatar');
        }

        connectedCallback() {
            this.shadowRoot.getElementById('photo').setAttribute('src', pathToPhotos + this.photo);
            this.shadowRoot.getElementById('avatar').setAttribute('src', pathToAvatars + this.avatar);
            this.shadowRoot.getElementById('name').innerHTML = this.name;
            this.shadowRoot.getElementById('experience').innerHTML = this.experience;
            this.shadowRoot.getElementById('country').innerHTML = this.country;
            this.shadowRoot.getElementById('place').innerHTML = this.place;
            this.shadowRoot.getElementById('title').innerHTML = this.title;
        }
    }
    window.customElements.define('single-observation', SingleObservation);
}
