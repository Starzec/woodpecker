fetch("../components/footer-component/footer.html")
    .then(stream => stream.text())
    .then(text => create_footer(text));

function create_footer(html) {
    class Footer extends HTMLElement {
        constructor() {
            super();
            let shadow = this.attachShadow({mode: 'open'});
            shadow.innerHTML = html;
        }
    }
    window.customElements.define('my-footer', Footer);
}
