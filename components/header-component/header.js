fetch("../components/header-component/header.html")
    .then(stream => stream.text())
    .then(text => create_header(text));

function create_header(html) {
    class Header extends HTMLElement {
        constructor() {
            super();
            let shadow = this.attachShadow({mode: 'open'});
            shadow.innerHTML = html;
        }
    }
    window.customElements.define('my-header', Header);
}


